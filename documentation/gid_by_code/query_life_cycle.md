# Query life cycle

### Routing
Все возможные маршруты прописаны в файле `server/routing.go`
```go
    func routing(e *echo.Echo, srv *server) {
	    route := e.Group("/")
	    route.POST("login", srv.loginHandler)
	    route.POST("select_game", srv.httpHandler(commands.CommandSelectGame))
	    route.POST("session", srv.httpHandler(commands.CommandSession))
	    route.POST("bet", srv.httpHandler(commands.CommandBet))
	    route.POST("refund", srv.httpHandler(commands.CommandRefund))
	    route.POST("win", srv.httpHandler(commands.CommandWin))
	    route.POST("balance", srv.httpHandler(commands.CommandGetBalance))
	    route.POST("close", srv.httpHandler(commands.CommandCloseSession))
    }
```
### httpHandler
Занимается обработкой http запросов. В этой функции прописана весь жизненный цикл запроса включающий в себя:

* Поиск и создание необходимой команды. Поиск осуществляется по переданной константе с именем команды 
```go
    cmd := commands.New(commandType)
	if cmd == nil {
		logger.Error("invalid command")
		return errors.InvalidCommand
	}
```
* Командой является структура, которая реализует следующие методы
```go
    //server/commands/command.go
    type Commander interface {
	    // Получение имени комманды
	    Command() string
	    // Получение имени комманды с префиксом `command.`
	    commandName() string
	    // Выполнение какого-то события по успешному выполнению комманды
	    Event() Eventer
	    // Подпись запроса
	    security.SignGetter
	    // Метод который необходимо реализовать у потомков, необходим для проверки подписи
	    security.Secretary
    }

```

* Получение всех необходимых для команды параметров.
```go
    // Bet command
    func (cmd *Bet) Eject(ctx echo.Context) error {
	    b := ctx.Request().Body
	    if err := json.NewDecoder(b).Decode(&cmd.Bets); err != nil {
		    return err
	    }
	    if err := cmd.Bets.Validate(); err != nil {
		    return err
	    }
	    return nil
    }
```
>  Для параметров которые передаются в заголовке запроса используется `ejectors`:
    
>>	* tokenEjector  
>>	* signEjector  
>>	* timeStampEjector  
>>	* countEjector  

* Валидация подписи 
``` go
        // server/handlers.go
		if !srv.tokenizer.ValidateSign(cmd, cmd) {
			return  errors.SignValidationFailed
		}
```
* Затем команда передается в функцию `execute`, где запустится функция которая будет обрабатывать эту команду и вернет результата обработки. 
```go
	// server/handlers.go
	evt, err := execute(cmd, srv.project)
	if err != nil {
		logger.Error("execute failed", zap.Error(err))
		return err
	}
```
Выбор функции которая будет осуществлять обработку запроса
```go
    // server/executor.go
	var err error
	switch com := cmd.(type) {
	case *commands.SelectGame:
    	err = prjApi.SelectGame(com)
	case *commands.Session:
		err = prjApi.GetGameSession(com)
	case *commands.Bet:
		err = prjApi.SetBet(com)
	case *commands.Refund:
		err = prjApi.Refund(com)
	case *commands.Win:
		err = prjApi.Win(com)
	case *commands.GetBalance:
		err = prjApi.Balance(com)
	case *commands.CloseSession:
		err = prjApi.Close(com)
	default:
		logger.Warn("command not supported")
		return nil, errors.CommandNotSupported
	}
```

* После выполнения функции в execute будет создана структура `Event`. которая должна быть реализована у каждой команды и должна возвращать структуру реализующую `Eventer`
```go
    type Eventer interface {
    	Event() string
	    Code() int //httpCode
	    GetData() interface{} // данные которые увидит пользователь на клиенте
    }
```

Пример реализации у команды `Bet`, где в Histories содержится сам ответ
 
 ```go
//server/commands/bet.go
 func (cmd *Bet) Event() Eventer {
	return newSuccess(cmd.name, http.StatusOK, cmd.Histories)
}

// server/commands/event.go
func newSuccess(name string, code int, data interface{}) *event {
	return newEvent(successName(name), code, data)
}
 ```
 
* Далее этот `Event` попадает в функцию `response` где извлекается значение `data` и возвращается `json`

* Эти самые `json` данные передаются в функцию `ctx.JSONBlob` - которая сформирует ответ с кодом `evt.Code()`


```go
 
    res, err := response("2.0.0", evt)
	if err != nil {
		logger.Error("response prepare failed", zap.Error(err))
		return err
	}
	return ctx.JSONBlob(evt.Code(), res)
```
---