# Balance

Этот	метод	вызывается	для	получения	баланса	игроков.

### URI
{project_url}/balance

---

### Query	parameters
| Parameter | Type | Description |
| ------ | ------ | ------ |
| 	count	 | 	int	 | Количество	транзакций |
| timestamp		| 	unix	timestamp | Временная	метка |
| [sign] | SHA1HEX | Подпись |

---

### Request	example
```json
[
	{
		"token":	"4d071214-c8f3-11e5-9956-625662870761",
		"transactionId":	"2f79f1ac-c8db-11e5-9956-625662870761"
	},
    {
		"token":	"c7ca038a-c9bb-11e5-9956-625662870761",
		"transactionId":	"c3ebc6c4-c9b9-11e5-9956-625662870761"
    }
]
```
### Request	fields
| Parameter | Type | Description |
| ------ | ------ | ------ |
| 	[token]	 | 		UUID		 | Идентификатор	игровой	сессии |
| 	transactionId	 | 	UUID	 | Идентификатор	транзакции|

---
### Success	response	example
```json
[
	{
		"transactionId":	"b531d0ba-e572-4d77-b1ea-18df1598b9a0",
    	"balance":	12230,
		"code":	1200
	},
	{
		"transactionId":	"c3ebc6c4-c9b9-11e5-9956-625662870761",
		"balance":	0,
		"code":	1404
	}
]
```

### Response	fields
| Parameter | Type | Description |
| ------ | ------ | ------ |
| 	transactionId	 | 		UUID		 | Идентификатор	транзакции|
| 	balance	 | 	float	 |  Баланс	игрока |
| 	code	 | 		int		 | Если	транзакция	обработана	без	ошибки,	используется	значение кода 1200 * |


[sign]: ../security/sign.MD
[token]: ../security/token.MD